** Project Structure**

* **grunt_tasks** - contains all Grunt tasks and their configuration files
    * **config** - Grunt task configuration
    * **env.local.json** - create and add this file with your project specific details
    * **environment-config.json** - rename to env.local.json
* **GruntFile.js** - the main Grunt file, you can add new build / workflow tasks specific to your project
* **composer.json** - add your project dependencies via composer
* **package.json** - add your Grunt task dependencies via npm


**Table of Contents**

[TOC]

# WordPress-CI #
This project is a WordPress application skeleton to use as part of a continuous integration pipeline. The goal is to provide a fully automated install process for WordPress using Composer and Grunt. It can also be used to setup your local dev enviroment.


# Why ?
I've recently started a gig that involves managing (and teaching about ) WordPress development. I've been struggling a bit with the lack of automation, testing and security. (I'm not saying WordPress is not secure, but it could be more secure by default). So I'm introducing my team to some Agile development best practices and this project is really just my own experiment to make sure the goals I have are actually possible with WordPress.

This project builds upon:

* [https://github.com/johnpbloch/wordpress](https://github.com/johnpbloch/wordpress)
* [http://wpackagist.org/](http://wpackagist.org/)
* [http://codex.wordpress.org/Giving_WordPress_Its_Own_Directory](http://codex.wordpress.org/Giving_WordPress_Its_Own_Directory)
* [https://roots.io/using-composer-with-wordpress/](https://roots.io/using-composer-with-wordpress/)


# Project Goals

* Eliminate all manual install and configuration steps to allow WordPress to be built as part of a CI pipeline
* Make use of best practices with default install
* Automate DB install/migration
* Must be configurable
* Must not edit WordPress core files so it remains update-proof

Ultimately I would love to have a project server folder that resembles this:
```
web-user-dir/
    ├─ wp/
    │    ├─ plugins/ -this may not be easy because of image/js/css deps :(
    │    ├─ wp-admin/
    │    ├─ wp-includes/ -this may not be easy because of image/js/css deps :(
    │    └─ wp-config.php
    └─ public/
          ├─ wp-content
          │    ├─ themes/
          │    └─ uploads/
          ├─ .htaccess
          └─ index.php
```

Currently it looks more like this:
```
web-user-dir/
    ├─ wp/
    │    └─ wp-config.php
    └─ public/
          ├─ wp/
          │    ├─ wp-admin/
          │    └─ wp-includes/
          ├─ wp-content
          │    ├─ themes/
          │    └─ uploads/
          ├─ .htaccess
          └─ index.php
```


# Getting Started

This is intended to install WordPress, project dependencies and a clean database so it can be built as part of a continuous integration pipeline.


## Clone wordpress-ci
```
$ git clone git@bitbucket.org:webgurl/wordpress-ci.git
$ cd wordpress-ci
```


## Configuration
Create an environment configuration file on the local machine. My setup assumes you are storing your environment specific data in Server Environment Variables. If you prefer a different setup you can easily modify the grunt tasks.

*filename:* ```grunt_tasks/env.local.json```
```javascript
{
  "debug": true,

  "wp": {
    "DB_NAME": "WP_DATABASE_NAME",
    "DB_USER": "WP_DATABASE_USER",
    "DB_PASSWORD": "WP_DATABASE_PASSWORD",
    "DB_HOST": "localhost:port",
    "CONTENT_PATH": "",
    "PLUGINS_PATH": "",
    "THEMES_PATH": ""
  }
}
```

* Create ```env.local.json``` in the ```grunt_tasks``` folder
* Copy/paste the above code snippet into the file and update with your environment variables
  * ```debug``` - will set value for ```WP_DEBUG```
  * ```wp.DB_NAME``` - sever environment variable for your wordpress database
  * ```wp.DB_USER``` - server environment variable for your wordpress database user
  * ```wp.DB_PASSWORD``` - server environment variable for your wordpress database password
  * ```wp.DB_HOST``` - ip or hostname of your database. usually ```localhost```


## Dependencies
* PHP >= 5.4
* Composer
* Node.js
* Grunt


## Database configuration
TODO


## Tests
TODO


## Run


### Add your WordPress specific environment variables to your machine.


### Create and edit env.local.json


### Install tools and workflow dependencies
Grunt is used to copy folders, update ```wp-config``` and migrate database files.

[Install Node](https://nodejs.org/)

[Install Grunt](http://gruntjs.com/getting-started)

Install grunt dependencies before running tasks
```bash
$ npm install
```


### Install project
```bash
$ cd wordpress-ci
$ composer install
```


# Tasks


## wp-install
```
$ cd wordpress-ci
$ grunt wp-install
```
Helper which calls:

* ```copy:wp-install```
* ```replace:wp-install```
* ```replace:wp-config```


## copy:wp-install
```
$ cd wordpress-ci
$ grunt copy:wp-install
```

Should be run after ```composer install```. This task will copy or move wordpress core files to a new location in an effort to create a more hardened install from the start of your project.

*Grunt task can be edited here:* ```wordpress-ci/grunt_tasks/copy.js```


## replace:wp-install
```
$ cd wordpress-ci
$ grunt replace:wp-install
```

Should be run after ```composer install```. This task will search ```wordpress-ci/wp/index.php``` and update the path according to [http://codex.wordpress.org/Giving_WordPress_Its_Own_Directory#Moving_a_Root_install_to_its_own_directory](http://codex.wordpress.org/Giving_WordPress_Its_Own_Directory#Moving_a_Root_install_to_its_own_directory).

Ideally I would prefer to use a placeholder in  ```index.php``` but I'm avoiding making changes to the core wordpress files so this project can stay update-proof.
```php
// what index.php currently displays
require( dirname( __FILE__ ) . '/wp-blog-header.php' );

// what I would prefer
require( dirname( __FILE__ ) . '@@WP_CORE/wp-blog-header.php' );
```

*Grunt task can be edited here:* ```wordpress-ci/grunt_tasks/replace.js```


## replace:wp-config
```
$ cd wordpress-ci
$ grunt replace:wp-config
```

Should be run after ```composer install```. This task will search ```wordpress-ci/wp/wp-config-sample.php``` and generates the wp-config.php in your build directory.

I have my servers setup to use Environment Variables to store the database access details, you are free to edit this task to suit your environment, although I do recommend keeping this sensitive info in environment variables for these 2 reasons:
* Allows you to more easily re-use the same configure for your dev, staging and prod environments
* If ```wp-config.php``` or ```env.local.json``` are accidentally committed to your repo, access to your database won't be compromised

Ideally I would prefer to use a placeholder in  ```wp-config-sample.php``` but I'm avoiding making changes to the core wordpress files so this project can stay update-proof.

*Grunt task can be edited here:* ```wordpress-ci/grunt_tasks/replace.js```


# Contribution guidelines 

* Writing tests
* Code review
* Other guidelines


# Who do I talk to? 

* Repo owner or admin
* Other community or team contact